---
layout: page
permalink: /about/
---

Writing code for <a class="link" href="https://glassdoor.com">Glassdoor</a> in Chicago. Go <a class="link" href="https://bryce.io">here</a> for more of me.
