---
layout: post
title: first post ☕️
tags: updates
---

Coffee has become a bit of an obsession for me as of late. I love finding new roasteries and experiencing different flavors but needed a place to keep track of them, and so this was born! While the majority of coffees I buy are from Chicago, I will sometimes delve into non-local roasteries.

With this blog, I hope to:

-   Keep a list of the coffees I've tried & and review them
-   Track qualities & sources of coffees to shape my decision-making
-   Create something simple, clean & easy to both post to and read/find

Thanks for reading, and I hope this helps you find better coffee!
