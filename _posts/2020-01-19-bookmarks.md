---
layout: post
title: bookmarks 📚
tags: [updates]
---

Instead of trying to fill this completely with my own content, I'm going to start posting bookmarks that I find interesting or helpful. This also provides a place to link to other coffee bloggers (that have _way_ more experience with this than me) as I discover them.

I added a [tag](/tag/bookmarks) to group them together. Hoping to grow it over time to eventually be a good source for learning!
