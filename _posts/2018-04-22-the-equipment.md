---
layout: post
title: equipment
---

How I'll be brewing for these reviews.

<div class='equipment'>
  <div class='item'>
    <img class="svg" src="{{ site.baseurl }}/images/chemex.png" alt="Icon by https://thenounproject.com/ericellis/collection/coffee" />
    <p>Chemex 6-cup <a href="https://amzn.to/2Jaop1m">pour-over</a>.</p>
  </div>

  <div class='item'>
    <img class="svg" src="{{ site.baseurl }}/images/kettle.png" alt="Icon by https://thenounproject.com/ericellis/collection/coffee" />
    <p>Willow & Everett <a href="https://amzn.to/2xHCjqs">electric kettle</a>.</p>
  </div>

  <div class='item'>
    <img class="svg" src="{{ site.baseurl }}/images/grinder.png" alt="Icon by https://thenounproject.com/ericellis/collection/coffee" />
    <p>Hario "Skerton" <a href="https://amzn.to/2xE3FxB"> coffee grinder</a>.</p>
  </div>

  <div class='item'>
    <img class="svg" src="{{ site.baseurl }}/images/aeropress.png" alt="Icon by https://thenounproject.com/ericellis/collection/coffee" />
    <p>Aerobie Aeropress <a href="https://amzn.to/2LUyLnL">espresso maker</a>.</p>
  </div>

  <div class='item'>
    <img class="svg" src="{{ site.baseurl }}/images/cup.png" alt="Icon by https://thenounproject.com/ericellis/collection/coffee" />
    <p>One of various mugs I've acquired over the years.</p>
  </div>
</div>
