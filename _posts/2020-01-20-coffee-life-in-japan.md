---
layout: post
title: coffee life in japan ⛩
tags: [bookmarks]
---

Hit another milestone with my coffee infatuation: bought a coffee (not table) [book](https://www.amazon.com/Coffee-Life-Japan-Merry-White/dp/0520271157). When I was last in Japan I stopped by some really cool coffee shops, but I was oblivious to the long history it has in the country.

![coffee life in japan](https://images-na.ssl-images-amazon.com/images/I/61ZBF3uqdoL.jpg)

Will share my thoughts on it when I finish it!
