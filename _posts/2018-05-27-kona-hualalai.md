---
layout: post
title: kona coffee purveyors ∙ hualalai
tags: [hawaii, kona]
---

The coffee in Hawaii was and is incredible. This was the first time I got the chance to try a bean near its origin, and it was the best tasting coffee I've had in a long time, possibly ever.

I tried an espresso shot with this roast and it had an excellent high note without the usual bitterness that I associate with espresso. It was so good that I (finally) bought an Aeropress so I could make it at home. I've been meaning to get one for a while, but having been on a pour-over kick I had no need for one. But now I'm hoping I can replicate that same shot with the beans I brought back.

{% include stars.html rating=5 %}

<div class="maps">
  {% include map.html id="source" class='left' title='Hualalai' lat='19.689707' lng='-155.864427' zoom='8' %}
  {% include map.html id="roastery" class='right' title='Kona Coffee Purveyors' lat='21.278876' lng='-157.825578' zoom='13' %}
</div>

_I couldn't actually find more info about this particular roast online, but I'm pretty sure the coffee comes from that^ mountain. There were cards in the store that had more info, I should've taken one!_
