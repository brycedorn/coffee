---
layout: post
title: colectivo ∙ malawi
tags: [milwaukee, colectivo]
---

I'm a big fan of Colectivo, but this one was just alright. Not bad by any means, but flavors were pretty mild and didn't stand out like some of the other roasts I've tried from there. Still a good roast, made for some great cold brews!

{% include stars.html rating=3.5 %}

<div class="maps">
{% include map.html id="source" class='left' title='Heza WS - Mutana' lat='-3.0077981' lng='29.6499162' zoom='7' %}
{% include map.html id="roastery" class='right' title='Colectivo Coffee' lat='41.922084' lng='-87.695938' zoom='13' %}
</div>

{% include info.html farm='Mzuzu Cooperative' process='Washed' region='Kayanza' variety='Bourbon' %}
