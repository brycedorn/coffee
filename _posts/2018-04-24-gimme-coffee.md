---
layout: post
title: gimme coffee ∙ rwanda dukunde kawa
tags: [nyc, rwanda]
---

This one's not a Chicago roastery, but I felt I had to give it some recognition as it's one of the best roasts I've ever tried. It has a wonderful aroma, and a bright acidity. I may start seeking out 'shade-grown' beans from now on, as it tasted incredible.

{% include stars.html rating=5 %}

<div class="maps">
{% include map.html id="source" class='left' title='Dukundu Kawa' lat='-1.6908586' lng='29.8352303' zoom='8' %}
{% include map.html id="roastery" class='right' title='Gimme! Coffee' lat='40.7223583' lng='-73.9950277' zoom='13' %}
</div>

{% include info.html farm='Dukundu Kawa' process='Washed' region='Gakenke District' variety='Bourbon' %}

If you're in the area, definitely check them out! I haven't personally been, but hoping to next time I'm in NYC.
