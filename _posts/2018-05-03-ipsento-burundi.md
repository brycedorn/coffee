---
layout: post
title: ipsento ∙ burundi
tags: [chicago, burundi]
---

Been meaning to try this place for a while - Ipsento is a well-known coffee shop in the Logan/Bucktown area, and for good reason! I went with the Burundi roast & it did not disappoint. Lovely fruit notes & low acidity lend to a pleasant & balanced cup.

{% include stars.html rating=4 %}

<div class="maps">
{% include map.html id="source" class='left' title='Heza WS - Mutana' lat='-3.0077981' lng='29.6499162' zoom='8' %}
{% include map.html id="roastery" class='right' title='Ipsento Coffee' lat='41.918639' lng='-87.687247' zoom='13' %}
</div>

{% include info.html farm='Heza WS - Mutana' process='Washed' region='Kayanza' variety='Bourbon' %}

And since it's starting to warm up, I decided to give cold brewing another shot. I've had some (absurdly) instense cold brews in the past, so I usually avoid them. But with the right control (I only let the grounds soak for ~18 hours), it creates a nice & soft flavor profile that is nice to sip and isn't overwhelmingly strong. I'll definitely be making more this summer!
